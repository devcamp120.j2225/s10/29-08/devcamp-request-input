//Import thư viện expressjs tương đương import express from "express"; 
const express = require("express");

//Khởi tạo 1 app express 
const app = express();

//Khai báo cổng chạy project
const port = 8000;

//Khai báo để app đọc được body json
app.use(express.json());

//Callback function là 1 function đóng vai trò là tham số của 1 function khác, nó sẽ được thực hiện khi function chủ được gọi
// Khai báo API dạng /
app.get("/", (req, res) => {
    let today = new Date();

    res.json({
        message: `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth()+1} năm ${today.getFullYear()}`
    })
})

// Request Params (Áp dụng cho GET, PUT, DELETE)
// Request params là những tham số xuất hiện trong URL của API
// VD: https://docs.google.com/presentation/d/108ppjKp_ac0ZHNfrNdOyxUPAG9E7Ehf_nKOFs-NniU8/edit
app.get("/request-params/:param1/:param2/param", (req, res) => {
    let param1 = req.params.param1;
    let param2 = req.params.param2;

    res.json({
        params: {
            param1,
            param2
        }
    })
})

//Request Query (Chỉ áp dụng cho phương thức GET)
//Khi lấy request query bắt buộc phải validate
app.get("/request-query", (req, res) => {
    let query = req.query;

    res.json({
        query
    })
})

// Request Body JSON (Chỉ áp dụng cho phương thức POST, PUT)
// Khi lấy request body bắt buộc phải validate
// Cần khai báo thêm ở dòng số 11
app.post("/request-body-json", (req, res) => {
    let body = req.body;

    res.json({
        body: body
    })
})

app.listen(port, () => {
    console.log("App listening on port: ", port);
})